<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 21.19
 */

class Muser extends CI_Model
{
    function getData()
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $query = $this->db->get();
        return $query->result_array();
    }

    function saveData()
    {
        $_POST['password'] = md5($_POST['password']);
        $this->db->insert_batch("tb_user", array($this->input->post()));
    }

    function deleteData()
    {
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $this->db->delete('tb_user');
    }

    function getUpdate($id)
    {
        $this->db->where("id", $id);
        $this->db->from('tb_user');
        $query = $this->db->get();
        return $query->row();
    }

    function updateData()
    {
        $_POST['password'] = md5($_POST['password']);
        $this->db->update_batch('tb_user', array($this->input->post()), 'id');
    }
}
