<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 21.19
 */
class Mbadan_usaha extends CI_Model
{
    function getData(){
        $this->db->select('*');
        $this->db->from('tb_monitoring');
        $query = $this->db->get();
        return $query->result_array();
    }
    function saveData(){
        $this->db->insert_batch("tb_monitoring", array($this->input->post()));
    }
    function deleteData(){
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $this->db->delete('tb_monitoring');
    }
    function getUpdate($id){
        $this->db->where("id",$id);
        $this->db->from('tb_monitoring');
        $query = $this->db->get();
        return $query->row();
    }

    function updateData(){
        print_r($this->input->post());
        $this->db->update_batch('tb_monitoring', array($this->input->post()), 'id');
    }
}
