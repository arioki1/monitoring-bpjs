<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 21.19
 */

class Mmonitoring extends CI_Model
{
    function getData(){
        $this->db->select('*');
        $this->db->from('tb_monitoring');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getUpdate($id){
        $this->db->where("id",$id);
        $this->db->from('tb_monitoring');
        $query = $this->db->get();
        return $query->row();
    }

    function updateData(){
        $this->db->update_batch('tb_monitoring', array($this->input->post()), 'id');
    }
}
