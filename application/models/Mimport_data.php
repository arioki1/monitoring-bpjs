<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 21.19
 */

class Mimport_data extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('Excel/PHPExcel', 'upload'));
        ini_set('max_execution_time', 3000);
        $this->load->model('Mbadan_usaha', 'badanusaha');
    }

    function saveData()
    {
        $config['upload_path'] = 'assets/excel';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '5120';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('file_excel')) {
            $output = array('status' => 'ERROR', 'message' => $this->upload->display_errors('<span>', '</span>'));
        } else {

            $file_excel = "./assets/excel/{$this->upload->file_name}";
            // Identifikasi File Excel Reader
            try {
                $excelReader = new PHPExcel_Reader_Excel2007();

                $loadExcel = $excelReader->load($file_excel);

                $sheet = $loadExcel->getActiveSheet()->toArray(null, true, true, true);
                // Loops Excel data reader
                foreach ($sheet as $key => $value) {
                    // Mulai Dari Baris ketiga
                    if ($key >= 7) {
                        $data = array(
                            'npp' => $value['B'],
                            'nama_badan_usaha' => $value['C'],
                            'blth_keps' => $value['D'],
                            'blth_na' => $value['E'],
                            'rate_jkk' => $value['F'],
                            'blth_terahir' => $value['G'],
                            'jumlah_terahir' => $value['H'],
                            'tk_aktif_terahir' => $value['I'],
                            'alamat' => $value['J'],
                            'kabupaten' => $value['K'],
                            'kode_area' => $value['L'],
                            'no_telp' => $value['M'],
                            'pic_nama' => $value['N'],
                            'pic_jabatan' => $value['O'],
                            'pic_no_hp' => $value['P'],
                            'pic_email' => $value['Q'],
                            'bl_th' => $value['R'],
                            'umur_piutang' => $value['S'],
                            'jlh_tk' => $value['T'],
                            'iuran' => $value['U'],
                            'denda' => $value['V'],
                            'total_iuran_denda' => $value['W'],
                            'status_piutang' => $value['X'],
                            'petugas_pemeriksa' => $value['Y'],
                            'status_kepesertaan' => $value['Z'],
                            'ro_ar' => $value['AA'],
                            'kategori_pelanggaran' => $value['AB'],
                            'tanggal_sp1' => $value['AC'],
                            'nomor_surat_sp1' => $value['AD'],
                            'tanggal_sp2' => $value['AE'],
                            'nomor_surat_sp2' => $value['AF'],
                            'tanggal_sp3' => $value['AG'],
                            'nomor_surat_sp3' => $value['AH'],
                            'tanggal_bak' => $value['AI'],
                            'tgl_pemeriksaan_data' => $value['AJ'],
                            'tgl_pemeriksaan' => $value['AK'],
                            'tgl_bap_spmi' => $value['AL'],
                            'tgl_thp' => $value['AM'],
                            'tgl_penyerahan_kpknl' => $value['AN'],
                            'tgl_penyerahan_kejaksaan' => $value['AO'],
                            'tgl_rikpadu_wasnaker' => $value['AP'],
                            'payroll' => $value['AQ'],
                            'patuh_na_cicil' => $value['AR'],
                            'bl_th_terakhir' => $value['AS'],
                            'potensi_tk' => $value['AT'],
                            'realisasi_tk' => $value['AU'],
                        );
                        $this->db->where("npp", $value['B']);
                        $query = $this->db->get('tb_monitoring');
                        if ($query->num_rows() == 0) {
                            $this->db->insert_batch("tb_monitoring", array($data));
                        }

                    }
                }
                unlink("./assets/excel/{$this->upload->file_name}");
            } catch (Exception $e) {
                redirect('admin/badanusaha');
                $output = array(
                    'status' => 'ERROR',
                    'message' => 'Error loading file "' . pathinfo($file_excel, PATHINFO_BASENAME) . '": ' . $e->getMessage()
                );
            }
            redirect('admin/badanusaha');
        }
    }
}
