<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 18.27
 */
if ($this->session->userdata('username') == "") {
    redirect(base_url("login"));
}
$this->load->view('_template/load');