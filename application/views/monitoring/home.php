<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.34
 */
?>
<!-- Panel Table Tools -->
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">Tabel Monitoring Data</h3>
    </header>
    <div class="panel-body">
        <table class="table table-hover dataTable table-striped w-full" id="exampleTableTools">
            <thead>
            <tr>
                <th>NO</th>
                <th>NPP</th>
                <th>Nama Badan Usaha</th>
                <th>Bl-Th</th>
                <th>Umur Piutang</th>
                <th>Jlh TK</th>
                <th>Iuran</th>
                <th>Denda</th>
                <th>Status Piutang</th>
                <th>Status Kepesertaan</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>NO</th>
                <th>NPP</th>
                <th>Nama Badan Usaha</th>
                <th>Bl-Th</th>
                <th>Umur Piutang</th>
                <th>Jlh TK</th>
                <th>Iuran</th>
                <th>Denda</th>
                <th>Status Piutang</th>
                <th>Status Kepesertaan</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<!-- End Panel Table Tools -->

