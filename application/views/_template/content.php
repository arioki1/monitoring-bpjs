<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 18.25
 */
?>
<!-- Page -->
<div class="page">
    <div class="page-content">
        <?php
        if (isset($content)){
            $this->load->view($content);
        }
        ?>
    </div>
</div>
<!-- End Page -->


