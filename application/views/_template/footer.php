<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 18.25
 */?>
<!-- Footer -->
<footer class="site-footer" style="visibility: hidden;">
    <div class="site-footer-legal">© 2018 Arioki Studio</a></div>
    <div class="site-footer-right">
        <a href="http://arioki1.github.io">Develop by Arioki Studio</a>
    </div>
</footer>
<script type="text/css">
    .site-footer {
        display: none;
    }
</script>