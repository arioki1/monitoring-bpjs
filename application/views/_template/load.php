<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 18.29
 */

$this->load->view('_template/header');
$this->load->view('_template/navbar');
$this->load->view('_template/content');
$this->load->view('_template/footer');
$this->load->view('_template/core');
