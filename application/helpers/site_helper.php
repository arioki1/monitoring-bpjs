<?php

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 12/12/2018
 * Time: 23.03
 */

if (!function_exists('log_all')) {
    function log_all()
    {
        if (ENVIRONMENT == 'production') {

        } else {
            log_message('error', "________________________________________________ START LOG ________________________________________________");
            $log1 = print_r(apache_request_headers(), true);
            $log2 = ($_SERVER['REQUEST_METHOD']);
            $log3 = print_r($_REQUEST, true);
            $log4 = print_r(apache_response_headers(), true);
            log_message('error', "________________________________________________  REQUEST   ________________________________________________");
            log_message('error', $log1);
            log_message('error', $log2 . " = " . $log3 . " ");
            log_message('error', "________________________________________________  REPONSE   ________________________________________________");
            log_message('error', $log4);
            log_message('error', '________________________________________________  END LOG  ________________________________________________');

        }
    }
}

if (!function_exists('log_app')) {
    /**
     * Menampilkan Log
     *
     *
     * @param   String $string Input Text
     */
    function log_app($string)
    {
        if (ENVIRONMENT == 'production') {

        } else {
            log_message('error', "IP : " . $_SERVER['REMOTE_ADDR'] . " Pesan : " . $string);
        }

    }
}
