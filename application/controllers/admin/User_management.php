<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.40
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class  User_management extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("muser", "user");
    }

    public function index()
    {
        $this->data = array(
            'content' => 'user_management/home',
            'js' => 'home_usermanagement',
            'css' => 'home_badan_usaha',
            'menu' => 'usermanagement',
            'title' => 'User Management',
            'data' => $this->user->getData()
        );

        $this->load->view('template', $this->data, FALSE);
    }

    public function add()
    {
        $this->data = array(
            'content' => 'user_management/add',
            'js' => 'add_badan_usaha',
            'css' => 'add_badan_usaha',
            'menu' => 'usermanagement',
            'title' => 'User Management',
        );
        $this->load->view('template', $this->data, FALSE);
    }

    public function save_data()
    {
        $this->user->saveData();
        redirect('admin/usermanagement');
    }

    function delete()
    {
        $this->user->deleteData();
        redirect('admin/usermanagement');
    }

    public function update()
    {
        $this->data = array(
            'content' => 'user_management/update',
            'js' => 'home_badan_usaha',
            'css' => 'home_badan_usaha',
            'menu' => 'usermanagement',
            'title' => 'User Management',
            'data' => $this->user->getUpdate($this->input->get('id'))
        );
        $this->load->view('template', $this->data, FALSE);
    }

    public function save_update()
    {
        $this->user->updateData();
        redirect('admin/usermanagement');
    }
}

/* End of file Controllername.php */
