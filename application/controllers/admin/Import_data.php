<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.40
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Import_data extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mimport_data", "importdata");
    }

    public function index()
	{
	    $this->data = array(
	        'content' => 'import_data/home',
            'js'      => 'home_import_data',
            'css'     => 'home_import_data',
            'menu'    => 'importdata',
            'title'   => 'Import Data',
        );

        $this->load->view('template', $this->data, FALSE);
	}

    public function save_data(){
        $this->importdata->saveData();
        //redirect('admin/badanusaha');
    }
}

/* End of file Controllername.php */
