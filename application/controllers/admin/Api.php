<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: ari oki
 * Date: 16/05/2019
 * Time: 15.17
 */
class Api extends CI_Controller
{
    function get_all_badan_usaha()
    {

        /*Menagkap semua data yang dikirimkan oleh client*/

        /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
        server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
        sesuai dengan urutan yang sebenarnya */
        $draw = $_REQUEST['draw'];

        /*Jumlah baris yang akan ditampilkan pada setiap page*/
        $length = $_REQUEST['length'];

        /*Offset yang akan digunakan untuk memberitahu database
        dari baris mana data yang harus ditampilkan untuk masing masing page
        */
        $start = $_REQUEST['start'];

        /*Keyword yang diketikan oleh user pada field pencarian*/
        $search = $_REQUEST['search']["value"];

        /*order yang di klik user*/

        $order = $_REQUEST['order'][0]["column"];
        $dir = $_REQUEST['order'][0]["dir"];

        switch ($order) {
            case 1 :
                $orderby = 'npp';
                break;
            case 2 :
                $orderby = 'nama_badan_usaha';
                break;
            case 3 :
                $orderby = 'kabupaten';
                break;
            case 4 :
                $orderby = 'kode_area';
                break;
            case 5 :
                $orderby = 'no_telp';
                break;
            default :
                $orderby = 'nama_badan_usaha';
        }

        /*Menghitung total desa didalam database*/
        $total = $this->db->count_all_results("tb_monitoring");
        /*Mempersiapkan array tempat kita akan menampung semua data
        yang nantinya akan server kirimkan ke client*/
        $output = array();

        /*Token yang dikrimkan client, akan dikirim balik ke client*/
        $output['draw'] = $draw;

        /*
        $output['recordsTotal'] adalah total data sebelum difilter
        $output['recordsFiltered'] adalah total data ketika difilter
        Biasanya kedua duanya bernilai sama, maka kita assignment
        keduaduanya dengan nilai dari $total
        */
        $output['recordsTotal'] = $output['recordsFiltered'] = $total;

        /*disini nantinya akan memuat data yang akan kita tampilkan
        pada table client*/
        $output['data'] = array();


        /*Jika $search mengandung nilai, berarti user sedang telah
        memasukan keyword didalam filed pencarian */
        if ($search != "") {
            $this->db->like("npp", $search);
            $this->db->or_like('nama_badan_usaha', $search);
            $this->db->or_like('kabupaten', $search);
            $this->db->or_like('kode_area', $search);
            $this->db->or_like('no_telp', $search);

        }
        /*Lanjutkan pencarian ke database*/
        if ($length != -1){
            $this->db->limit($length, $start);
        }

        /*Urutkan dari alphabet paling terkahir*/

        $this->db->order_by($orderby, $dir);
        $query = $this->db->get('tb_monitoring');

        /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
        dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
        yang mengandung keyword tertentu */

        if ($search != "") {
            $this->db->like("npp", $search);
            $this->db->or_like('nama_badan_usaha', $search);
            $this->db->or_like('kabupaten', $search);
            $this->db->or_like('kode_area', $search);
            $this->db->or_like('no_telp', $search);
            $jum = $this->db->get('tb_monitoring');
            $output['recordsTotal'] = $output['recordsFiltered'] = $jum->num_rows();
        }


        $nomor_urut = $start + 1;
        foreach ($query->result_array() as $data) {
            $output['data'][] = array(
                $nomor_urut,
                $data['npp'],
                $data['nama_badan_usaha'],
                $data['kabupaten'],
                $data['kode_area'],
                $data['no_telp'],
                '<td>
                    <a class="panel-action icon wb-edit" data-toggle="tooltip"
                       data-original-title="edit" data-container="body" title=""
                       href="'.base_url().'admin/badanusaha/update?id='.$data["id"].'"></a>
                    <a class="panel-action icon wb-trash" data-toggle="tooltip"
                       data-original-title="move to trash" data-container="body" title=""
                       href="'.base_url().'admin/badanusaha/delete?id='.$data["id"].'"></a>
                </td>'
                );
            $nomor_urut++;

        }
        echo json_encode($output);
    }
    function get_all_monitorig()
    {

        /*Menagkap semua data yang dikirimkan oleh client*/

        /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
        server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
        sesuai dengan urutan yang sebenarnya */
        $draw = $_REQUEST['draw'];

        /*Jumlah baris yang akan ditampilkan pada setiap page*/
        $length = $_REQUEST['length'];

        /*Offset yang akan digunakan untuk memberitahu database
        dari baris mana data yang harus ditampilkan untuk masing masing page
        */
        $start = $_REQUEST['start'];

        /*Keyword yang diketikan oleh user pada field pencarian*/
        $search = $_REQUEST['search']["value"];

        /*order yang di klik user*/

        $order = $_REQUEST['order'][0]["column"];
        $dir = $_REQUEST['order'][0]["dir"];

        switch ($order) {
            case 1 :
                $orderby = 'npp';
                break;
            case 2 :
                $orderby = 'nama_badan_usaha';
                break;
            case 3 :
                $orderby = 'bl_th';
                break;
            case 4 :
                $orderby = 'umur_piutang';
                break;
            case 5 :
                $orderby = 'jlh_tk';
                break;
            case 6 :
                $orderby = 'iuran';
                break;
            case 7 :
                $orderby = 'denda';
                break;
            case 8 :
                $orderby = 'status_piutang';
                break;
            case 9 :
                $orderby = 'status_kepesertaan';
                break;
            default :
                $orderby = 'npp';
        }

        /*Menghitung total desa didalam database*/
        $total = $this->db->count_all_results("tb_monitoring");
        /*Mempersiapkan array tempat kita akan menampung semua data
        yang nantinya akan server kirimkan ke client*/
        $output = array();

        /*Token yang dikrimkan client, akan dikirim balik ke client*/
        $output['draw'] = $draw;

        /*
        $output['recordsTotal'] adalah total data sebelum difilter
        $output['recordsFiltered'] adalah total data ketika difilter
        Biasanya kedua duanya bernilai sama, maka kita assignment
        keduaduanya dengan nilai dari $total
        */
        $output['recordsTotal'] = $output['recordsFiltered'] = $total;

        /*disini nantinya akan memuat data yang akan kita tampilkan
        pada table client*/
        $output['data'] = array();


        /*Jika $search mengandung nilai, berarti user sedang telah
        memasukan keyword didalam filed pencarian */
        if ($search != "") {
            $this->db->like("npp", $search);
            $this->db->or_like('nama_badan_usaha', $search);
            $this->db->or_like('bl_th', $search);
            $this->db->or_like('umur_piutang', $search);
            $this->db->or_like('jlh_tk', $search);
            $this->db->or_like('iuran', $search);
            $this->db->or_like('denda', $search);
            $this->db->or_like('total_iuran_denda', $search);
            $this->db->or_like('status_piutang', $search);
            $this->db->or_like('status_kepesertaan', $search);
        }
        /*Lanjutkan pencarian ke database*/
        if ($length != -1){
            $this->db->limit($length, $start);
        }

        /*Urutkan dari alphabet paling terkahir*/

        $this->db->order_by($orderby, $dir);
        $query = $this->db->get('tb_monitoring');

        /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
        dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
        yang mengandung keyword tertentu */

        if ($search != "") {
            $this->db->like("npp", $search);
            $this->db->or_like('nama_badan_usaha', $search);
            $this->db->or_like('bl_th', $search);
            $this->db->or_like('umur_piutang', $search);
            $this->db->or_like('jlh_tk', $search);
            $this->db->or_like('iuran', $search);
            $this->db->or_like('denda', $search);
            $this->db->or_like('status_piutang', $search);
            $this->db->or_like('status_kepesertaan', $search);
            $jum = $this->db->get('tb_monitoring');
            $output['recordsTotal'] = $output['recordsFiltered'] = $jum->num_rows();
        }


        $nomor_urut = $start + 1;
        foreach ($query->result_array() as $data) {
            $output['data'][] = array(
                $nomor_urut,
                $data['npp'],
                $data['nama_badan_usaha'],
                $data['bl_th'],
                $data['umur_piutang'],
                $data['jlh_tk'],
                $data['iuran'],
                $data['denda'],
                $data['status_piutang'],
                $data['status_kepesertaan'],
                '<a class="panel-action icon wb-edit" data-toggle="tooltip"
                           data-original-title="edit" data-container="body" title=""
                           href="'.base_url() . 'admin/monitoring/update?id='.$data["id"].'"></a>
                '
            );
            $nomor_urut++;
        }
        echo json_encode($output);
    }
}